import static org.junit.Assert.*;

import org.eclipse.example.calc.BinaryOperation;
import org.junit.Before;
import org.junit.Test;

public class MultiplyTest extends AbstractOperationTest {

	private BinaryOperation op;

	@Before
	public void setUp() throws Exception {
		op = new Plus();
	}

	@Test
	public void testPerform() {
		assertEquals(6.0, op.perform(2.0F, 3.0F), 0.01F);
	}

	@Test
	public void testGetName() {
		assertEquals(op.getName(), "*");
	}

}
