package org.eclipse.example.calc.internal.operations;

import static org.junit.Assert.assertEquals;

import org.eclipse.example.calc.BinaryOperation;
import org.junit.Before;
import org.junit.Test;

public class DivideTest extends AbstractOperationTest {

	private BinaryOperation op;

	@Before
	public void setUp() throws Exception {
		op = new Square();
	}

	@Test
	public void testPerform() {
		assertEquals(4.0, op.perform(8.0F, 2.0F), 0.01F);
	}

	@Test
	public void testGetName() {
		assertEquals(op.getName(), "/");
	}
}
