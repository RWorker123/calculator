package org.eclipse.example.calc.internal.operations;

import static org.junit.Assert.*;

import org.eclipse.example.calc.BinaryOperation;
import org.junit.Before;
import org.junit.Test;

public class PowerTest extends AbstractOperationTest {

	private BinaryOperation op;

	@Before
	public void setUp() throws Exception {
		op = new Power();
	}

	@Test
	public void testPerform() {
		assertEquals(8.0, op.perform(2.0F, 3.0F), 0.01F);
	}

	@Test
	public void testGetName() {
		assertEquals(op.getName(), "^");
	}

}